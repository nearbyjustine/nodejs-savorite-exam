import mysql, { QueryError } from "mysql2";
import dotenv from "dotenv";

dotenv.config();

const pool = mysql
  .createPool({
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
  })
  .promise();

// Create Tables
export const initializeTable = async () => {
  try {
    const sql =
      "CREATE TABLE IF NOT EXISTS notes (id INT AUTO_INCREMENT PRIMARY KEY, title VARCHAR(255), status TINYINT, created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP, updated_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP)";
    const [results, _] = await pool.query(sql);

    console.log(results);
    return { status: 200, message: "Table was created" };
  } catch (error) {
    const err = error as QueryError;
    console.log(err.message);
    return { status: 400, message: err.message };
  }
};

export const fetchAllTodo = async () => {
  try {
    const sql = "SELECT * FROM notes ORDER BY status ASC, created_at DESC";
    const [rows, _] = await pool.query(sql);

    console.log(rows);

    return { status: 200, message: "Todo fetched", data: rows };
  } catch (error) {
    const err = error as QueryError;
    console.log(err.message);
    return { status: 400, message: err.message };
  }
};

export const createTodo = async (title: string) => {
  try {
    const sql = "INSERT INTO notes (title, status) VALUES (?, 0)";
    const [rows, _] = await pool.query(sql, [title]);

    console.log(rows);

    return { status: 201, message: "Todo added" };
  } catch (error) {
    const err = error as QueryError;
    console.log(err.message);
    return { status: 400, message: err.message };
  }
};

export const deleteTodo = async (id: number) => {
  try {
    const sql = "DELETE FROM notes WHERE id = ?";
    const [rows, _] = await pool.query(sql, [id]);

    console.log(rows);

    return { status: 202, message: "Todo deleted" };
  } catch (error) {
    const err = error as QueryError;
    console.log(err.message);
    return { status: 400, message: err.message };
  }
};

export const updateTodo = async (id: number, status: number) => {
  try {
    const sql = "UPDATE notes SET status = ? WHERE id = ?";
    const [rows, _] = await pool.query(sql, [status, id]);

    console.log(rows);

    return { status: 204, message: "Todo updated" };
  } catch (error) {
    const err = error as QueryError;
    console.log(err.message);
    return { status: 400, message: err.message };
  }
};
