import express from "express";
import bodyParser from "body-parser";
import { Request, Response } from "express";
import dotenv from "dotenv";
import { createTodo, deleteTodo, fetchAllTodo, initializeTable, updateTodo } from "./db.js";
dotenv.config();

const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// cors
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

// nitialize table
app.get("/", (req: Request, res: Response) => {
  initializeTable();
});

// get all todos
app.get("/todo", async (req: Request, res: Response) => {
  try {
    const todos = await fetchAllTodo();
    res.json(todos).status(200);
  } catch (err) {
    res.json(err).status(400);
  }
});

// add todo
app.post("/todo", async (req: Request, res: Response) => {
  try {
    const data: { title: string } = req.body;
    const result = await createTodo(data.title);
    res.json(result).status(201);
  } catch (err) {
    res.json(err).status(401);
  }
});

// delete todo
app.delete("/todo", async (req: Request, res: Response) => {
  try {
    const data: { id: number } = req.body;
    const result = await deleteTodo(data.id);
    res.json(result).status(202);
  } catch (err) {
    res.json(err).status(400);
  }
});

// update todo
app.put("/todo", async (req: Request, res: Response) => {
  try {
    const data: { id: number; status: number } = req.body;
    const result = await updateTodo(data.id, data.status);
    res.json(result).status(204);
  } catch (err) {
    res.json(err).status(400);
  }
});

app.listen(process.env.PORT || 8000, () => {
  console.log("Listening to PORT", process.env.PORT || 8000);
});
